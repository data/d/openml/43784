# OpenML dataset: IMDb-Ratings

https://www.openml.org/d/43784

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset contains IMDb ratings and votes information for movies having original title. 
Useful for creating top rated movies recommender system.
Content
Descriptions of the columns:
titleId (string) - alphanumeric unique identifier of the title
title (string)  the original title
averageRating  weighted average of all the individual user ratings
numVotes - number of votes the title has received
Acknowledgements
Thank you IMDb for providing the details of numerous movies.
Inspiration
Using this dataset you can see which are highest rated movies currently.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43784) of an [OpenML dataset](https://www.openml.org/d/43784). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43784/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43784/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43784/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

